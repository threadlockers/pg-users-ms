# pg-users-ms

The users microservice for the PlantaGotchi app. It handles everything from registration to authentication and account editing.