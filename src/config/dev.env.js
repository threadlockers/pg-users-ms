const merge = require('webpack-merge');
const prodEnv = require('./prod.env');

module.exports = merge(prodEnv, {
    NODE_ENV: '"development"',
    port: 7001,
    secret: 'secret',
    session_secret: 'session_secret',
});