const db = require('../db');

function addCredits(username, credits, callback) {
    db.query('SELECT credits FROM user WHERE BINARY username = ?', [username], (err, result) => {
        if (err) return callback(err, {message: 'User not found!'});
        let newCreditsValue = result[0].credits + credits;
        db.query('UPDATE user SET credits = ? WHERE BINARY username = ?', [newCreditsValue, username], (err, result) => {
            if (err) {
                return callback(err, {message: 'Internal error!'});
            }
                return callback(null, { message: `Added ${credits} credits to user ${username}.\n New credits value: ${newCreditsValue}` });
        });
    });
}
module.exports = {
    addCredits
};