const jwt = require('jsonwebtoken');
const config = require('./../config');

module.exports = function (req, res, next) {
    let clientSessionToken = req.get('Authorization');
    if (clientSessionToken !== undefined) {
        jwt.verify(clientSessionToken, config.env.session_secret, (err, decoded) => {
            if (err) return res.status(401).send({
                message: 'Identity can\'t be verified!'
            });
            if (decoded === undefined) return res.stauts(401).send({
                message: 'Identity can\'t be verified!'
            });
            next();
        });
    }
    else return res.status(401).send({
        message: 'Identity can\'t be verified!'
    });
}