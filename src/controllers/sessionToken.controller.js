const jwt = require('jsonwebtoken');
const config = require('./../config');

module.exports = function (username, callback) {
    let tokenPayload = {
        username
    };
    jwt.sign(tokenPayload, config.env.session_secret, (err, token) => {
        if(err) throw err;
        callback(null, token);
    });
}