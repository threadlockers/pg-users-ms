const db = require('../db');
const bcrypt = require('bcryptjs');

module.exports.create = function(data, callback) {
    bcrypt.genSalt(10, function(err, salt) {
        if(err) throw err;
        bcrypt.hash(data.password, salt, function(err, hash){
            if(err) throw err;

            let values = [data.username, hash];

            db.query('INSERT INTO user (username, password) VALUES (?, ?)', values, function(err, result) {
                if (err) return callback(err);
                callback(null, result);
            });

        });
    });
};

module.exports.authenticate = function(data, callback) {
        db.query('SELECT * FROM user WHERE BINARY username = ?', data.username, function(err, result){
            if(err) throw err;
            if(result[0] === undefined) callback(null, undefined);
            result = {...result[0]};
            bcrypt.compare(data.password, result.password, (err, passwordMatches) => {
                if(err) throw err;
                if(!passwordMatches) callback(null, undefined);
                callback(null, true);
            });
        });
};

module.exports.getUserData = function(username, callback) {
    db.query('SELECT * FROM user WHERE BINARY username=?', [username], function(err, result) {
            if(err) return callback(err);
            result = {...result[0]};
            result = {
                credits: result.credits,
                inventory_id: result.inventory_id,
            }
            callback(null, result);
    });
}