const express = require('express');
// const morgan = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
const users = require('./models/users');
const config = require('./config');

Promise = require('bluebird');

//Setting up express + bodyParser and other middleware
const ms = express();

// ms.use(morgan('combined'));
ms.use(bodyParser.json());
ms.use(bodyParser.text());
ms.use(cors());

//Defining routes
const apiEndpoint = require('./routes/api');

ms.use('/api', apiEndpoint);

const port = process.env.PORT || config.env.port;

ms.listen(port, '0.0.0.0', () => {
    console.log(`Users Microservice Running on port ${port}`);
});