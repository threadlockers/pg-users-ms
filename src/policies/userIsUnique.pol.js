const bodyParser = require('body-parser');
const db = require('../db');

module.exports = function(req, res, next) {
    const userToBeValidated = req.body.username;
    db.query('SELECT EXISTS(SELECT 1 FROM user WHERE BINARY username = ?)', userToBeValidated, function(err, result){
        if (err) return res.status(500).send({message: "User could not be validated!"});
        if(result[0][`EXISTS(SELECT 1 FROM user WHERE BINARY username = \'${userToBeValidated}\')`] == 1){
            return res.status(409).send({message: 'User already exists!'});
        }
        next();
    });
};