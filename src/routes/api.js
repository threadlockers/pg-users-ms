const express = require('express');
const router = express.Router();
const db = require('../db');
const jwt_controller = require('../controllers/jwt.controller');
const issueSessionToken = require('../controllers/sessionToken.controller');
const isAuthenticated = require('../controllers/isAuthenticated.controller');
const User = require('../models/users');
const bodyParser = require('body-parser');
const userIsUnique = require('./../policies/userIsUnique.pol');
const inputIsValid = require('./../policies/inputIsValid.pol');
const creditsController = require('./../controllers/creditsController.controller');

router.post('/new-user', jwt_controller, inputIsValid, userIsUnique, (req, res) => {
    User.create({username: req.body.username, password: req.body.password}, (err, result) => {
        if(err) return res.status(500).send({message: 'User not registered!'});
        return res.status(200).send({message: 'User registered successfully!'});
    });
});

router.post('/authenticate-user', jwt_controller, inputIsValid, (req, res) => {
    User.authenticate({username: req.body.username, password: req.body.password}, (err, credentialsAreCorrect) => {
        if(err) throw err;
        if(!credentialsAreCorrect) res.status(401).send({message: 'Wrong username/password combination!'});
        issueSessionToken(req.body.username, (err, token) => {
            if(err) throw err;
            res.status(200).send(token);
        });
    });
});

router.post('/add-credits', jwt_controller, (req, res) => {
    //Expected input
    //{
    //  "username": "Dorel",
    //  "package":  "100/250/600/1200"
    //}
    creditsController.addCredits(req.body.username, req.body.package, (err, message) => {
        if(err) return res.status(500).send(message);
        return res.status(200).send(message);
    });
});

router.post('/user-data', isAuthenticated, jwt_controller, (req, res) => {
    User.getUserData(req.body.username, (err, result) => {
        if(err) return res.status(500).send({message: 'Internal error!'});
        return res.status(200).send(result);
    });
});

router.get('/protected', isAuthenticated, (req, res) => {
    res.status(200).send({message:'Cool'});
});

module.exports = router;